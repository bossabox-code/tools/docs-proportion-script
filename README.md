This is a generalist script to calculate documentation proportion (Lines of documentation / Total lines of code).

Requirements:

cloc (https://github.com/AlDanial/cloc)

jq (https://stedolan.github.io/jq/)


Usage:

You can insert as many files as you need

`./script.sh doc_file1.html docs/doc_file2.html`


Make sure the script have the permissions to run:

`chmod +x script.sh`
