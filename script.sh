#!/bin/sh

# This script requires the following commands to run:
#   cloc (https://github.com/AlDanial/cloc)
#   jq (https://stedolan.github.io/jq/)

if test $# -eq 0 ; then
    echo "Usage: $0  doc filenames"
    exit
fi

EXCLUDE_DOCS_FILES_SCRIPT="cloc HEAD --json "
DOCS_SUM=0

for FILE in "$@"
do
  EXCLUDE_DOCS_FILES_SCRIPT+="--not-match-f=$FILE "
  DOCS_SUM+=$(wc -l < $FILE)
done

SUM=$($EXCLUDE_DOCS_FILES_SCRIPT | jq ".SUM.code")

if test $DOCS_SUM -eq 0 ; then
  echo 0
  exit
else
  echo $(echo "$DOCS_SUM/$SUM" | bc -l)
fi
